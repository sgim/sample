package sample.my.com.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout llMain;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private LinearLayout llRoot;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private InputMethodManager imm;
    private CoordinatorLayout clMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        llRoot = (LinearLayout) findViewById(R.id.ll_root);
        llRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        editTextEmail = (EditText) findViewById(R.id.edit_text_email);
        editTextPassword = (EditText) findViewById(R.id.edit_text_password);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsedappbar);

        llMain = (LinearLayout) findViewById(R.id.ll_main);

        Map<Integer, String> buttonMap = new HashMap<>();
        buttonMap.put(0, "0. Snackbar Test");
        setButton(buttonMap);

    }

    private void setButton(Map<Integer, String> buttonMap) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        int position = 0;
        for (int key : buttonMap.keySet()) {
            View view = inflater.inflate(R.layout.item_button, null);
            Button btnSample = (Button) view.findViewById(R.id.btn_sample);
            btnSample.setTag(key);
            btnSample.setText(buttonMap.get(key));
            btnSample.setOnClickListener(this);
            llMain.addView(view, position);
            position++;
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        Class subClass = null;
        switch ((int) view.getTag()) {
            case 0:
                break;
        }
        if (subClass != null) {
            intent = new Intent(MainActivity.this, subClass);
            startActivity(intent);
        }
        setSnackbar(view, "Snackbar Test Message [Tag : " + (int) view.getTag() + "]");
    }

    private void setSnackbar(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }
}
